// Global variables
const canvas = document.getElementById("canvas1");
const ctx = canvas.getContext("2d");
function sizeCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}
window.addEventListener("resize", sizeCanvas);
sizeCanvas();

var gameIsGoing = false;
document.getElementById("pause-btn").addEventListener("click", function() {
    gameIsGoing = !gameIsGoing;
    if (gameIsGoing) {
        document.getElementById("pause-btn").textContent = "Unpause";
    } else {
        document.getElementById("pause-btn").textContent = "Pause";
        window.requestAnimationFrame(function() { game.loop() });
    }
});

// Models

class Game {
    // Game class.

    // Init
    constructor(asteroidNum) {
        // Generate asteroids
        this.asteroids = [];
        for (let i = 0; i < asteroidNum; i++) {
            this.asteroids.push(Asteroid.genAsteroid());
        }

        // Set up player
        this.player = new Player(window.innerWidth / 2, window.innerHeight / 2);

        // Set number of times player was hit by asteroids
        this.hits = 0;
        document.getElementById("hits").textContent = this.hits;
    }

    // Logic
    updateLogic() {
        // Player
        this.player.updateLogic();

        // Asteroids
        this.asteroids.forEach((asteroid, index) => {
            // Move the asteroid
            asteroid.updateLogic();
            if (asteroid.isOutOfBounds()) {
                // Substitute old asteroids with new asteroids
                this.substituteAsteroid(index);
            }

            // Detect collisions with player
            // . Measure distance from this asteroid to player
            // . If distance is small enough, take a life off
            let dist_player_asteroid_x = this.player.x - asteroid.x;
            let dist_player_asteroid_y = this.player.y - asteroid.y;
            let dist_player_asteroid = Math.sqrt(dist_player_asteroid_x * dist_player_asteroid_x + dist_player_asteroid_y * dist_player_asteroid_y);
            if (dist_player_asteroid < 40) {
                this.loseLife();
                this.substituteAsteroid(index);
            }
        });
    }

    substituteAsteroid(index) {
        delete this.asteroids[index];
        this.asteroids[index] = Asteroid.genAsteroid();
    }

    loseLife() {
        this.hits++;
        document.getElementById("hits").textContent = this.hits;
    }

    // Draw
    draw() {
        // Asteroids
        for (let asteroid of this.asteroids) {
            asteroid.draw();
        }
        // Player
        this.player.draw();
    }

    // Loop
    loop() {
        // Update logic
        this.updateLogic();

        // Clear screen
        ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);

        // Draw
        this.draw();
        
        // Go again
        if (!gameIsGoing) {
            window.requestAnimationFrame(function() { game.loop() });
        //window.requestAnimationFrame(game.loop);
        }
    }
}

class Asteroid {
    // Asteroid class.

    // Init
    constructor(startx, starty, angle, speed = 5) {
        this.x = startx;
        this.y = starty;
        this.angle = angle;
        this.speed = speed;
    }

    static genAsteroid() {
        // Generate an asteroid pointing in the right direction [^genAsteroids].

        // . Choose position (lrud). X: left

        // . Choose start coordinates.
        // -- Left: x = 0; y = randFloat(0, innerHeight);
        // -- Right: x = innerWidth; y = randFloat(0, innerHeight);
        // -- Up: x = randFloat(0, innerWidth); y = 0;
        // -- Down: x = randFloat(0, innerWidth); y = innerHeight;

        // . Choose angle.
        // -- Left: randFloat(-1/4, 1/4) * tau;
        // -- Right: randFloat(1/4, 3/4) * tau;
        // -- Up: randFloat(0, 1/2) * tau;
        // -- Down: randFloat(1/2, 1) * tau;

        let pos = choice(["left", "right", "up", "down"]);

        let startx;
        let starty;
        let angle;

        if (pos === "left") {
            startx = 0;
            starty = randFloat(0, window.innerHeight);
            angle = randFloat(-1/4, 1/4) * tau;
        } else if (pos === "right") {
            startx = window.innerWidth;
            starty = randFloat(0, window.innerHeight);
            angle = randFloat(1/4, 3/4) * tau;
        } else if (pos === "up") {
            startx = randFloat(0, window.innerWidth);
            starty = 0;
            angle = randFloat(1/2, 1) * tau;
        } else {
            startx = randFloat(0, window.innerWidth);
            starty = window.innerHeight;
            angle = randFloat(0, 1/2) * tau;
        }

        return new Asteroid(startx, starty, angle);
    }

    // Logic
    updateLogic() {
        this.x +=  Math.cos(this.angle) * this.speed;
        // It is -= since origin is at top left, not bottom left.
        this.y -= Math.sin(this.angle) * this.speed;
    }

    isOutOfBounds() {
        let extraMargin = 20;
        return (this.x < 0 - extraMargin) // past left border
            || (this.x > window.innerWidth + extraMargin) // past right border
            || (this.y < 0 - extraMargin) // past up border
            || (this.y > window.innerHeight + extraMargin); // past down border
    }

    // Draw
    draw() {
        let asteroidColor = "#558cf4";
        let asteroidRadius = 30; // 15
        ctx.fillStyle = asteroidColor;
        ctx.beginPath();
        ctx.arc(this.x, this.y, asteroidRadius, 0, tau);
        ctx.fill();
    }
}

class Player {
    constructor(startx, starty) {
        this.x = startx;
        this.y = starty;
        this.dx = 0;
        this.dy = 0;
    }

    updateLogic() {
        // Drag
        let dragCoeff = 0.9
        this.dx *= dragCoeff;
        this.dy *= dragCoeff;

        // Put a cap on max speed
        let max_speed_val = 100;
        if (Math.abs(this.dx) > max_speed_val) {
            this.dx = Math.sign(this.dx) * max_speed_val;
        }
        if (Math.abs(this.dy) > max_speed_val) {
            this.dy = Math.sign(this.dy) * max_speed_val;
        }

        // Change position by speed
        this.x += this.dx;
        this.y += this.dy;

        // Stay in bounds
        if (this.x > window.innerWidth) {
            this.x = window.innerWidth;
        }
        if (this.x < 0) {
            this.x = 0;
        }
        if (this.y > window.innerHeight) {
            this.y = window.innerHeight;
        }
        if (this.y < 0) {
            this.y = 0;
        }
    }

    draw() {
        let playerColor = "#ff0000"
        let playerRadius = 10;
        ctx.fillStyle = playerColor;
        ctx.beginPath();
        ctx.arc(this.x, this.y, playerRadius, 0, tau);
        ctx.fill();
    }
}

function KeyboardController(keys, repeat) {
    // key codes => timer ID | null (no repeat)
    let timers = {};

    // When a key starts being pressed, call the key action callback
    // and set a timer to generate another one after a delay.
    document.onkeydown = function(event) {
        let key = event.keyCode;
        if (!(key in keys))
            return true;
        if (!(key in timers)) {
            timers[key] = null;
            keys[key]();
            if (repeat !== 0)
                timers[key] = setInterval(keys[key], repeat);
        }
        return false;
    };

    // Cancel timeout and mark key as released on keyup
    document.onkeyup = function(event) {
        let key = event.keyCode;
        if (key in timers) {
            if (timers[key] !== null)
                clearInterval(timers[key]);
            delete timers[key];
        }
    };

    // When a window is unfocused, cancel all held keys.
    // This will prevent keys from being 'stuck down'.
   window.onblur = function () {
       for (key in timers)
           if (timers[key] !== null)
               clearInterval(timers[key]);
       timers = {};
   };
};


let asteroidNum = 50;
let game = new Game(50);

let moveDist = 3;
KeyboardController({
    37: function() { game.player.dx -= moveDist},
    39: function() { game.player.dx += moveDist},
    38: function() { game.player.dy -= moveDist},
    40: function() { game.player.dy += moveDist}
}, 50);

// https://stackoverflow.com/questions/4011793/this-is-undefined-in-javascript-class-methods
//window.requestAnimationFrame(game.loop);
window.requestAnimationFrame(function() { game.loop() });

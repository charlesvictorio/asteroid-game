# Asteroid game

![Game diagram](game_diagram.png)

## How to play
- You control the red player.
- Use the arrow keys to dodge the blue asteroids.
- You can see the number of times you got hit in the top left.
- There is a pause button in the top right corner.

## What I learned
- I used **html** and **css** for building all of the parts and putting them in position.
- I used **javascript**:
	- **object oriented programming** for the game structure.
	- **canvas** to draw the player and asteroids.
	- **event listeners** to navigate the player with key presses.
- I used **science**:
    - **physics** to calculate the player's velocity.
    - **trigonometry** to calculate the angle the asteroids should go.

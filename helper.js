const tau = 2 * Math.PI;

// Random

function randFloat(min, max) {
    // [min, max)
    return Math.random() * (max - min) + min;
}

function randInt(min, max) {
    // [min, max]
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function choice(choices) {
    let index = Math.floor(Math.random() * choices.length);
    return choices[index];
}
